import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter/material.dart';


typedef void CameraXCreatedCallback(FlutterCameraXController controller);

class FlutterCameraX extends StatefulWidget {
  final CameraXCreatedCallback onFlutterCameraXCreated;

  FlutterCameraX({this.onFlutterCameraXCreated,});

  @override
  State<StatefulWidget> createState() => _FlutterCameraXState();
}

class _FlutterCameraXState extends State<FlutterCameraX> {
  @override
  Widget build(BuildContext context) {
    return AndroidView(
      viewType: 'com.example.camera/camera',
      onPlatformViewCreated: _onPlatformViewCreated,
    );
  }

  void _onPlatformViewCreated(int id) {
    if (widget.onFlutterCameraXCreated == null) {
      return;
    }
    widget.onFlutterCameraXCreated(new FlutterCameraXController._(id));
  }
}

class FlutterCameraXController {
  FlutterCameraXController._(int id) : _channel = new MethodChannel('com.example.camera/camera_$id');
  final MethodChannel _channel;

  Future<String> capture() async {
    return await _channel.invokeMethod<String>('camera#capture');
  }
  Future<void> setFlash(bool bool) async {
    return await _channel.invokeMethod<String>('camera#setFlash',{
      "flash": bool
    });
  }
}