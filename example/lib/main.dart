import 'package:flutter/material.dart';
import 'package:fluttercamerax/fluttercamerax.dart';

void main() => runApp(MaterialApp(home: FlutterCameraXControllerExample()));

class FlutterCameraXControllerExample extends StatefulWidget {
  @override
  _FlutterCameraXControllerState createState() => _FlutterCameraXControllerState();
}

class _FlutterCameraXControllerState extends State<FlutterCameraXControllerExample> {
  FlutterCameraXController controller;

  bool flash = false;

  void _setFlash(){
    setState(() {
      flash = !flash;
    });
    controller.setFlash(flash);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Camera example')),
        body: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: double.infinity,
              child: FlutterCameraX(
                onFlutterCameraXCreated: _onFlutterCameraXCreated,
              ),
            ),

            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: IconButton(
                  icon: Icon(Icons.camera, color: Colors.white, size: 50,),
                  onPressed: () =>
                      controller.capture().then((path) =>
                      {
                        showDialog(
                            context: context,
                            child: AlertDialog(
                              title: Text("Result"),
                              content: Text(path),
                            ))
                      }),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                margin: EdgeInsets.all(10),
                child: IconButton(
                  icon: Icon( flash ? Icons.flash_on : Icons.flash_off, color: Colors.white, size: 30),
                  onPressed: _setFlash,
                ),
              ),
            )],
        )
    );
  }

  void _onFlutterCameraXCreated(FlutterCameraXController controller) {
    this.controller = controller;
  }
}
