package com.example.fluttercamerax

import android.app.Activity
import android.content.Context
import androidx.lifecycle.Lifecycle
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.StandardMessageCodec
import io.flutter.plugin.platform.PlatformView
import io.flutter.plugin.platform.PlatformViewFactory

class CameraFactory (private val messenger: BinaryMessenger,
                        private val activity: Activity,
                        private val lifecycle: Lifecycle,
                        private val permissionsRegistry : PermissionsRegistry) : PlatformViewFactory(StandardMessageCodec.INSTANCE) {

        override fun create(context: Context?, viewId: Int, args: Any?): PlatformView {
            val builder = CameraBuilder()
            return builder.build(viewId, context!!, messenger, activity, lifecycle,permissionsRegistry)
        }
    }

