package com.example.fluttercamerax

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.media.MediaScannerConnection
import android.view.View
import android.webkit.MimeTypeMap
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.google.common.util.concurrent.ListenableFuture
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.platform.PlatformView
import java.io.File
import java.util.concurrent.Executors

class CameraController(
        id: Int,
        private val context: Context,
        messenger: BinaryMessenger,
        private val activity: Activity,
        private val lifecycle: Lifecycle,
        private val permissionsRegistry: PermissionsRegistry) : DefaultLifecycleObserver, PlatformView, MethodChannel.MethodCallHandler {

    val PERMISSIONS_REQUEST_CODE = 10

    private val permissionsRequired = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    )

    private val methodChannel: MethodChannel = MethodChannel(messenger, "com.example.camera/camera_$id")

    private var disposed = false

    //Initialize CameraX
    private lateinit var cameraProvider: ProcessCameraProvider
    private val cameraProviderFuture: ListenableFuture<ProcessCameraProvider> = ProcessCameraProvider.getInstance(context)
    private val cameraSelector: CameraSelector = CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build()
    private lateinit var camera: Camera
    private lateinit var imageCapture: ImageCapture
    private val executor = Executors.newSingleThreadExecutor()
    lateinit var file: File

    private var previewView = PreviewView(context)

    init {
        methodChannel.setMethodCallHandler(this)

        lifecycle.addObserver(this)

        if (allPermissionsGranted()) {
            startCamera()
        } else {
            permissionsRegistry.addListener(CameraRequestPermissionsListener(object : ResultCallback {
                override fun onResult(errorCode: String?, errorDescription: String?) {
                    if (errorCode == null)
                        startCamera()
                }
            }))

            ActivityCompat.requestPermissions(activity, permissionsRequired, PERMISSIONS_REQUEST_CODE)
        }
    }

    override fun onMethodCall(methodCall: MethodCall, result: MethodChannel.Result) {
        when (methodCall.method) {
            "camera#capture" -> {
                file = File(activity.externalMediaDirs.first(), "${System.currentTimeMillis()}.jpg")
                val outputFileOptions = ImageCapture.OutputFileOptions.Builder(file).build()

                imageCapture.takePicture(outputFileOptions, executor, object : ImageCapture.OnImageSavedCallback {
                    override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                        activity.runOnUiThread {
                            scanMedia(context,file.absolutePath)
                            result.success(file.absolutePath)
                        }
                    }

                    override fun onError(exception: ImageCaptureException) {
                        exception.printStackTrace()
                        activity.runOnUiThread {
                            result.error("capture", "Error capturing the photo", null)
                        }
                    }
                })
            }
            "camera#setFlash" -> {
                val flash = methodCall.argument<Boolean>("flash") ?: return
                if (flash) {
                    imageCapture.flashMode = ImageCapture.FLASH_MODE_ON
                } else {
                    imageCapture.flashMode = ImageCapture.FLASH_MODE_OFF
                }
            }
            else -> result.notImplemented()
        }
    }


    override fun getView(): View {
        return previewView
    }

    private fun startCamera() {
        previewView.post {
            //Initialize CameraX
            cameraProviderFuture.addListener(Runnable {
                cameraProvider = cameraProviderFuture.get()
                // Set up the preview use case to display camera preview
                val preview = Preview.Builder().apply {
                    setTargetAspectRatio(AspectRatio.RATIO_16_9)
                    setTargetRotation(previewView.display.rotation)
                }.build()
                preview.setSurfaceProvider(previewView.previewSurfaceProvider)

                // Set up the capture use case to allow users to take photos
                imageCapture = ImageCapture.Builder().apply {
                    setTargetAspectRatio(AspectRatio.RATIO_16_9)
                    setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                    setTargetRotation(previewView.display.rotation)
                }.build()

                // Must unbind the use-cases before rebinding them
                cameraProvider.unbindAll()

                // Apply declared configs to CameraX using the same lifecycle owner
                try {
                    camera = cameraProvider.bindToLifecycle(activity as LifecycleOwner, cameraSelector, preview, imageCapture)
                } catch (exc: Exception) {
                    exc.printStackTrace()
                }
                //addOnTouchListener()
            }, ContextCompat.getMainExecutor(context))
        }
    }

    private fun allPermissionsGranted(): Boolean {
        val listPermissions = ArrayList<Int>().apply {
            add(ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA))
            add(ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            add(ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE))
        }
        for (permission in listPermissions) {
            if (permission != PackageManager.PERMISSION_GRANTED) return false
        }
        return true
    }

    fun scanMedia(context: Context, filePath: String) {
        val absPath = arrayOf(filePath)
        val mimeType = arrayOf(MimeTypeMap.getSingleton().getMimeTypeFromExtension(filePath.substring(filePath.lastIndexOf("."))))
        MediaScannerConnection.scanFile(context, absPath, mimeType, null)
    }


    override fun dispose() {
        if (disposed) {
            return
        }
        disposed = true
        methodChannel.setMethodCallHandler(null)
    }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        executor.shutdown()
    }
}