package com.example.fluttercamerax

import androidx.annotation.NonNull
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.embedding.engine.plugins.lifecycle.FlutterLifecycleAdapter

import io.flutter.plugin.common.PluginRegistry

/** FluttercameraxPlugin */
class FlutterCameraXPlugin: DefaultLifecycleObserver,FlutterPlugin, ActivityAware {
  private val VIEW_TYPE = "com.example.camera/camera"
  private var pluginBinding: FlutterPlugin.FlutterPluginBinding? = null
  private lateinit var lifecycle: Lifecycle

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    pluginBinding = flutterPluginBinding
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    pluginBinding = null
  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    lifecycle = FlutterLifecycleAdapter.getActivityLifecycle(binding)
    lifecycle.addObserver(this)

    pluginBinding!!
            .platformViewRegistry
            .registerViewFactory(VIEW_TYPE, CameraFactory(
                    pluginBinding!!.binaryMessenger,
                    binding.activity,
                    lifecycle,
                    object : PermissionsRegistry {
                      override fun addListener(handler: PluginRegistry.RequestPermissionsResultListener) {
                        binding.addRequestPermissionsResultListener(handler)
                      }
                    }
            ))
  }
  override fun onDetachedFromActivity() { lifecycle.removeObserver(this)}

  override fun onDetachedFromActivityForConfigChanges() { onDetachedFromActivity() }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
    lifecycle = FlutterLifecycleAdapter.getActivityLifecycle(binding)
    lifecycle.addObserver(this)
  }

}