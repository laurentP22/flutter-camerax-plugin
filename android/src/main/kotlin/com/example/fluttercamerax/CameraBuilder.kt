package com.example.fluttercamerax

import android.app.Activity
import android.content.Context
import androidx.lifecycle.Lifecycle
import io.flutter.plugin.common.BinaryMessenger

class CameraBuilder {
    fun build(id: Int, context: Context, binaryMessenger: BinaryMessenger, activity: Activity,lifecycle: Lifecycle, permissionsRegistry : PermissionsRegistry): CameraController {
        return CameraController(id, context, binaryMessenger, activity, lifecycle,permissionsRegistry)
    }
}