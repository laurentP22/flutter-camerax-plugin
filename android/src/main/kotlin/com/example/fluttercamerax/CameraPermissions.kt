package com.example.fluttercamerax


import android.content.pm.PackageManager
import io.flutter.plugin.common.PluginRegistry

interface PermissionsRegistry {
    fun addListener(handler: PluginRegistry.RequestPermissionsResultListener)
}

interface ResultCallback {
    fun onResult(errorCode: String?, errorDescription: String?)
}

open class CameraRequestPermissionsListener(private val callback: ResultCallback) : PluginRegistry.RequestPermissionsResultListener {
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>?, grantResults: IntArray): Boolean {
        if (requestCode == 10) { // If request is cancelled, the result arrays are empty.
            if (grantResults.isNotEmpty()) {
                for (result in grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        callback.onResult("cameraPermission", "Permission permission not granted")
                        return true
                    }
                }
                callback.onResult(null, null)
            }
        }else
            callback.onResult("cameraPermission", "Permission permission not granted")
        return true
    }
}