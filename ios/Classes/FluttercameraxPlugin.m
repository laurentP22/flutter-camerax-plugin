#import "FluttercameraxPlugin.h"
#if __has_include(<fluttercamerax/fluttercamerax-Swift.h>)
#import <fluttercamerax/fluttercamerax-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "fluttercamerax-Swift.h"
#endif

@implementation FluttercameraxPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFluttercameraxPlugin registerWithRegistrar:registrar];
}
@end
